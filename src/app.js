const express = require('express');

const app = express();

//Configuration Server
app.set('port', process.env.PORT || 3000);


//Middlewares
app.use(express.json());


//Routes
app.use(require('./routes/animales'));
app.use(require('./routes/empleados'));
app.use(require('./routes/usuarios'));
app.use(require('./routes/areas'));



app.listen(app.get('port'), () =>{
    console.log('Servidor activo en puerto '+ app.get('port'));
})