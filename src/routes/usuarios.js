const { request, response } = require("express");
const express = require("express");
const cors = require('cors');


const router = express.Router();

router.use(cors());
const mysqlConnection = require("../database");

 //GET

router.get("/api/todosUsuarios", (resquest, response) => {
  mysqlConnection.query("SELECT b.idUsuarios, a.nombre, a.edad, a.sexo, b.numBoleto FROM SerVivo as a  INNER JOIN Usuarios as b  ON a.idSerVivo = b.idUsuarios;",
   (err, rows, fields) => {
    if (!err) {
      response.json(rows);
    } else {
      console.log(err);
    }
  });
});

router.get("/api/soloUsuario/:id", (request, response) => {
  const id = request.params.id;

  mysqlConnection.query(
    "SELECT b.idUsuarios, a.nombre, a.edad, a.sexo, b.numBoleto FROM SerVivo as a  INNER JOIN Usuarios as b  ON a.idSerVivo = b.idUsuarios WHERE idUsuarios = ?;",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json(rows);
      } else {
      }
    }
  );
});


//POST
router.post("/api/guardarUsuario", async (request, response) => {
  const nombre = request.body.nombre;
  const edad = request.body.edad;
  const sexo = request.body.sexo;
  const numBoleto = request.body.numBoleto;
  

  mysqlConnection.query(
    "INSERT INTO SerVivo (nombre, edad, sexo) VALUES (?,?,?);",
    [nombre, edad, sexo],
    (err, rows, fields) => {
      if (!err) {
        const id = rows.insertId;
      
        mysqlConnection.query(
        "INSERT INTO Usuarios (idUsuarios, numBoleto) VALUES (?,?);",
        [id, numBoleto ],
        (err, rows, fields) => {
          if (!err) {
            response.json({
              msg: "Usuario agregado con éxito"
            });
          } else {
            response.json(err);
          }
        }
      );
      } else {
        response.json(err);
      }
    }
  );


});

//PUT

router.put("/api/modificarUsuario/:id", (request, response) => {
    const id = request.params.id;
    const nombre = request.body.nombre;
    const edad = request.body.edad;
    const sexo = request.body.sexo;
    const numBoleto = request.body.numBoleto;
    
  
    mysqlConnection.query(
      "UPDATE SerVivo SET nombre = ?, edad = ?, sexo = ? WHERE idSerVivo = ?",
      [nombre, edad, sexo, id],
      (err, rows, fields) => {
        if (!err) {
         
          
  
          mysqlConnection.query(
          "UPDATE Usuarios SET numBoleto = ? WHERE idUsuarios = ?",
          [ numBoleto, id ],
          (err, rows, fields) => {
            if (!err) {
              response.json({
                msg: "Usuario modificado correctamente"
              });
            } else {
              response.json(err);
            }
          }
        );
        } else {
          response.json(err);
        }
      }
    );


});


//DELETE

router.delete("/api/eliminarUsuario/:id", (request, response) => {
  const id = request.params.id;
  mysqlConnection.query(
    "DELETE FROM SerVivo WHERE idSerVivo = ?",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json({
          msg: "Usuario eliminado correctamente"
        })
      } else {
        response.json(err);
      }
    }
  ); 
});

module.exports = router;
