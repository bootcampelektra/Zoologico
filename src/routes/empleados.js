const { request, response } = require("express");
const express = require("express");
const router = express.Router();

const mysqlConnection = require("../database");

 //GET

router.get("/api/todosEmpleados", (resquest, response) => {
  mysqlConnection.query("SELECT b.idEmpleados, a.nombre, a.edad, a.sexo, b.puesto, b.salario FROM SerVivo as a  INNER JOIN Empleados as b  ON a.idSerVivo = b.idEmpleados;",
   (err, rows, fields) => {
    if (!err) {
      response.json(rows);
    } else {
      console.log(err);
    }
  });
});

router.get("/api/soloEmpleado/:id", (request, response) => {
  const id = request.params.id;

  mysqlConnection.query(
    "SELECT b.idEmpleados, a.nombre, a.edad, a.sexo, b.puesto, b.salario FROM SerVivo as a  INNER JOIN Empleados as b  ON a.idSerVivo = b.idEmpleados WHERE a.idSerVivo = ?",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json(rows);
      } else {
      }
    }
  );
});


//POST
router.post("/api/guardarEmpleado", async (request, response) => {
  const nombre = request.body.nombre;
  const edad = request.body.edad;
  const sexo = request.body.sexo;
  const puesto = request.body.puesto;
  const salario = request.body.salario;
  

  mysqlConnection.query(
    "INSERT INTO SerVivo (nombre, edad, sexo) VALUES (?,?,?);",
    [nombre, edad, sexo],
    (err, rows, fields) => {
      if (!err) {
        const id = rows.insertId;
     

        mysqlConnection.query(
        "INSERT INTO Empleados (idEmpleados, puesto, salario) VALUES (?,?,?);",
        [id, puesto, salario ],
        (err, rows, fields) => {
          if (!err) {
            response.json({
              msg: "Empleado agregado correctamente"
            });
          } else {
            response.json(err);
          }
        }
      );
      } else {
        response.json(err);
      }
    }
  );


});

//PUT

router.put("/api/modificarEmpleado/:id", (request, response) => {
    const id = request.params.id;
    const nombre = request.body.nombre;
    const edad = request.body.edad;
    const sexo = request.body.sexo;
    const puesto = request.body.puesto;
    const salario = request.body.salario;
    
  
    mysqlConnection.query(
      "UPDATE SerVivo SET nombre = ?, edad = ?, sexo = ? WHERE idSerVivo = ?",
      [nombre, edad, sexo, id],
      (err, rows, fields) => {
        if (!err) {
          
    
  
          mysqlConnection.query(
          "UPDATE Empleados SET salario = ?, puesto = ? WHERE idEmpleados = ?",
          [  salario, puesto ,id ],
          (err, rows, fields) => {
            if (!err) {
              response.json({
                msg: "Empleado modificado con éxito"
              });
            } else {
              response.json(err);
            }
          }
        );
        } else {
          response.json(err);
        }
      }
    );


});


//DELETE

router.delete("/api/eliminarEmpleado/:id", (request, response) => {
  const id = request.params.id;
  mysqlConnection.query(
    "DELETE FROM SerVivo WHERE idSerVivo = ?",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json({
          msg: "Empleado eliminado"
        })
      } else {
        response.json(err);
      }
    }
  ); 
});

module.exports = router;
