const { request, response } = require("express");
const express = require("express");
const router = express.Router();

const mysqlConnection = require("../database");

 //GET

router.get("/api/todasAreas", (resquest, response) => {
  mysqlConnection.query("SELECT * FROM Areas;",
   (err, rows, fields) => {
    if (!err) {
      response.json(rows);
    } else {
      console.log(err);
    }
  });
});

router.get("/api/solaArea/:id", (request, response) => {
  const id = request.params.id;

  mysqlConnection.query(
    "SELECT * FROM Areas WHERE idAreas = ?;",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json(rows);
      } else {
      }
    }
  );
});


//POST
router.post("/api/guardarArea", async (request, response) => {
  const nombre = request.body.nombre;
  const ubicacion = request.body.ubicacion;
  

  mysqlConnection.query(
    "INSERT INTO Areas (nombre, ubicacion) VALUES (?,?);",
    [nombre, ubicacion],
    (err, rows, fields) => {
      if (!err) {
        response.json({
            msg: "Area registrada correctamente"
        });
      } else {
        response.json(err);
      }
    }
  );


});

//PUT

router.put("/api/modificarArea/:id", (request, response) => {
    const id = request.params.id;
    const nombre = request.body.nombre;
    const ubicacion = request.body.ubicacion;
  
    mysqlConnection.query(
      "UPDATE Areas SET nombre = ?, ubicacion = ?  WHERE idAreas = ?",
      [nombre, ubicacion, id],
      (err, rows, fields) => {
        if (!err) {
          response.json({
            msg: "Area modificada correctamente"
          });
        } else {
          response.json(err);
        }
      }
    );


});


//DELETE

router.delete("/api/eliminarArea/:id", (request, response) => {
  const id = request.params.id;
  mysqlConnection.query(
    "DELETE FROM Areas WHERE idAreas = ?",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json({
          msg: "Area eliminada correctamente"
        })
      } else {
        response.json(err);
      }
    }
  ); 
});

module.exports = router;
