const { request, response } = require("express");
const express = require("express");
const router = express.Router();

const mysqlConnection = require("../database");

 //GET

router.get("/api/todosAnimales", (resquest, response) => {
  mysqlConnection.query("SELECT b.idAnimales, a.nombre, a.edad, a.sexo, b.especie FROM SerVivo as a  INNER JOIN Animales as b  ON a.idSerVivo = b.idAnimales;",
   (err, rows, fields) => {
    if (!err) {
      response.json(rows);
    } else {
      console.log(err);
    }
  });
});

router.get("/api/soloAnimal/:id", (request, response) => {
  const id = request.params.id;

  mysqlConnection.query(
    "SELECT b.idAnimales, a.nombre, a.edad, a.sexo, b.especie FROM SerVivo as a  INNER JOIN Animales as b  ON a.idSerVivo = b.idAnimales WHERE idAnimales = ?",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json(rows);
      } else {
      }
    }
  );
});


//POST
router.post("/api/guardarAnimal", async (request, response) => {
  const nombre = request.body.nombre;
  const edad = request.body.edad;
  const sexo = request.body.sexo;
  const especie = request.body.especie;
  

  mysqlConnection.query(
    "INSERT INTO SerVivo (nombre, edad, sexo) VALUES (?,?,?);",
    [nombre, edad, sexo],
    (err, rows, fields) => {
      if (!err) {
        const id = rows.insertId;
        
       

        mysqlConnection.query(
        "INSERT INTO Animales (idAnimales, especie) VALUES (?,?);",
        [id ,especie],
        (err, rows, fields) => {
          if (!err) {
            response.json({
              msg: "Animal agregado con éxito"
            });
          } else {
            response.json(err);
          }
        }
      );
      } else {
        response.json(err);
      }
    }
  );


});

//PUT

router.put("/api/modificarAnimal/:id", (request, response) => {
  const id = request.params.id;
  const nombre = request.body.nombre;
  const edad = request.body.edad;
  const sexo = request.body.sexo;
  const especie = request.body.especie;
  

  mysqlConnection.query(
    "UPDATE SerVivo SET nombre = ?, edad = ?, sexo = ? WHERE idSerVivo = ?",
    [nombre, edad, sexo, id],
    (err, rows, fields) => {
      if (!err) {
        
        mysqlConnection.query(
        "UPDATE Animales SET especie = ? WHERE idAnimales = ?",
        [especie, id],
        (err, rows, fields) => {
          if (!err) {
            response.json({
              msg: "Animal actualizado con éxito"
            });
          } else {
            response.json(err);
          }
        }
      );
      } else {
        response.json(err);
      }
    }
  );




});


//DELETE

router.delete("/api/eliminarAnimal/:id", (request, response) => {
  const id = request.params.id;
  mysqlConnection.query(
    "DELETE FROM SerVivo WHERE idSerVivo = ?",
    [id],
    (err, rows, fields) => {
      if (!err) {
        response.json({
          msg: "Animal Eliminado"
        })
      } else {
        response.json(err);
      }
    }
  ); 
});

module.exports = router;
